import java.util.ArrayList;


public class ContaInvestimentos extends Conta
{
    
    private ArrayList<Investimento> investimentos;
    
    public ContaInvestimentos(int numero) {
        super(numero); 
        investimentos = new ArrayList();
    }
    
    
    public void aplicar(Investimento investimento) {
        if(super.getSaldo() >= investimento.getValorInvestido()){
            super.sacar(investimento.getValorInvestido());
            investimentos.add(investimento);
        }
    }
    
    public void resgatar(Investimento resgate) {
         //se eu nao quiser resgatar tudo como ficaria?
            super.depositar(resgate.valorAtualInvestimento ());
            investimentos.remove(resgate);
        
    }
    
    
    @Override
    public double getSaldo(){
        
        double soma = 0;
        
        for (Investimento i : investimentos) {
            soma += i.valorAtualInvestimento() ;  
        }
        
        return soma + super.getSaldo();
    }
    
    public ArrayList<Investimento> getInvestimentos() {
        return this.investimentos;   
    }
}
