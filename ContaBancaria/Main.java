import java.util.Scanner;


public class Main
{
    static Scanner leOut = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        Scanner le = new Scanner(System.in);
        Cliente cli = cadastraCliente(le);
        
        cli.addConta(new Poupanca(123));
        cli.addConta(new ContaCorrente(456));
        
        while(true) {
    
            int menu = imprimeMenu();
            if(menu == 1){
               cli.addConta( criaConta() );
            } else if(menu == 5){
                break;
            } else if(menu == 2){
                realizarDeposito(cli);
            } else if(menu == 3){
                realizarSaque(cli);
            } else if(menu == 4){
                System.out.println("# Mostrar Saldo: R$ " + cli.saldoTotal());
            }
         }
    }
    
    public static void realizarSaque(Cliente cliente) {
        Scanner le = new Scanner(System.in);
        System.out.println("# Saque");
        System.out.println("# Escolha uma conta");
        int posicao = 1;
         for(Conta c : cliente.getContas()) {
             System.out.println("### " +(posicao++)+" conta: " + c.getNumero());
        }
        
        int contaEscolhida = le.nextInt();
        
        System.out.println("# Informe o valor a ser retirado: ");
        double valor = le.nextDouble();
        
        cliente.getContas().get(contaEscolhida - 1).sacar(valor);
    }
    
    public static void realizarDeposito(Cliente cliente) {
         //listar as contas
        Scanner le = new Scanner(System.in);
        System.out.println("# Deposito");
        System.out.println("# Escolha uma conta");
        int posicao = 1;
         for(Conta c : cliente.getContas()) {
             System.out.println("### " +(posicao++)+" conta: " + c.getNumero());
        }
        
        int contaEscolhida = le.nextInt();
        
        System.out.println("# Informe o valor a ser depositado: ");
        double valor = le.nextDouble();
        
        cliente.getContas().get(contaEscolhida - 1).depositar(valor);
    }
    
    public static Conta criaConta(){
        Scanner le = new Scanner(System.in);
        System.out.println("# Criação de Conta");  
        System.out.println("# Informe o tipo de conta");
        System.out.println("# (P)oupança, (C)onta Corrente");
        String op = le.next();
        
        Conta c = null;
        System.out.println("# número da conta");
        int numeroConta = le.nextInt();
        
        if(op.equalsIgnoreCase("P")) {
            c = new Poupanca(numeroConta);
        } else if(op.equalsIgnoreCase("C")) {
            c = new ContaCorrente(numeroConta);
        } 
           
        return c;
        
    }
    
    public static int imprimeMenu() {
        System.out.println("### MENU ###");
        System.out.println("#1) Criar Conta");
        System.out.println("#2) Depositar");
        System.out.println("#3) Sacar");
        System.out.println("#4) Mostrar Saldo");
        System.out.println("#5) Sair");  
        return leOut.nextInt();
    }
    
    public static Cliente cadastraCliente(Scanner le) {
        Cliente cli = new Cliente();
        System.out.println("Nome do cliente");
        cli.setNome(le.next());  
        
        return cli;
    }
}
